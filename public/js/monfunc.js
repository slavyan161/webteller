function formatNumber(n,decpoint,gpoint,decdigit){
	var num=''+n;
	var retnum='';
	var pos=num.indexOf('.');
	var intFrac='';
	var decFrac='';
	//alert(n);
	if(pos>-1){
		intFrac=num.substr(0,pos);
		decFrac=num.substr(pos+1,decdigit);
	}
	else{
		intFrac=num;
		decFrac='00';
	}
	if (gpoint==' ')
	{
		retnum=intFrac+(decdigit > 0 ? decpoint : '')+(decdigit > 0 ? decFrac : '');
	}
	else{
		//alert(intFrac+' '+decFrac);
		i=intFrac.length-3;
		j=3;
		num=''
		while(i>-3){
			if(i<0){
				j=3+i;
				i=0;
			}
			num=gpoint+intFrac.substr(i,j)+num;
			//alert(intFrac.substr(i,j));
			i-=3;

		}
		num=num.substr(1,num.length-1);
		retnum=num+(decdigit > 0 ? decpoint : '')+(decdigit > 0 ? decFrac : '');
	}
	return retnum;
} // end of formatNumber

function ResetDeposit(sPPID, sAccountNumber, sAccountDesc)
{
  var bCancel = false;
  var iDeposit = 0;
  while ((!bCancel) && (Math.abs(iDeposit) == 0))
  {
    Deposit = window.prompt("Masukkan jumlah permohonan penambahan deposit", "0");
    if (Deposit)
      iDeposit = parseInt(Deposit);
    else bCancel = true;
    //alert(iDeposit);
  }
  if (Math.abs(iDeposit) > 0)
  {
    if (Math.abs(iDeposit) > 0)
    {
      if (window.confirm("Anda yakin mau mengajukan permohonan penambahan nilai deposit untuk loket " + sPPID + " sebesar Rp. " + formatNumber(iDeposit,',', '.', 0) + " melalui rekening "+ sAccountDesc +"-" + sAccountNumber + " ?"))
        window.open ('onpays-deposit-req.php?_p=' + sPPID + '&_v=' + iDeposit + '&_acntbank=' + sAccountNumber);
    }
    else alert('Permohonan penambahan deposit dibatalkan');
  }
  else alert('Permohonan penambahan deposit dibatalkan');
}

function ResetDeposit_v2(sPPID, sAccountNumber, iDeposit)
{
  if (Math.abs(iDeposit) > 0)
  {
    if (Math.abs(iDeposit) > 0)
    {
      if (window.confirm("Anda yakin mau mengajukan permohonan penambahan nilai deposit untuk loket " + sPPID + " sebesar Rp. " + formatNumber(iDeposit,',', '.', 0) + " melalui rekening " + sAccountNumber + " ?"))
        window.open ('onpays-deposit-req.php?_p=' + sPPID + '&_v=' + iDeposit + '&_acntbank=' + sAccountNumber);
    }
    else alert('Permohonan penambahan deposit dibatalkan');
  }
  else alert('Permohonan penambahan deposit dibatalkan');
}



function BlockPP(iC, sPPID)
{
  var s = '';
  if (iC == 101) s = 'Anda yakin mau membuka blok PP ' + sPPID + ' sehingga dapat bertransaksi kembali?';
  else s = 'Anda yakin mau mengeblok PP ' + sPPID + ' sehingga tidak dapat bertransaksi?';
  if (window.confirm(s))
    window.open('onpays-pp-block.php?_p=' + sPPID + '&_c=' + iC);
}
function ShowDepositHistory(sPPID)
{
  window.open('onpays-deposit-hist.php?_p=' + sPPID + '&_n=' + 20) ;
}

function OpenFileDebt(sFile)
{
  window.open('onpays-file-debt.php?_p=' + sFile + '&_n=' + 20) ;
}

function ShowBalanceSheet(sPPID,sStartDate,sEndDate)
{
  window.open('onpays-deposit-transaction-balance.php?sEndDate=' + sEndDate + '&sStartDate=' + sStartDate + '&_p=' + sPPID + '&_n=' + 20);
}

function ApproveDeposit(sRID, sPPID, sDepo, sNL)
{
var s = 'Anda menyetujui penambahan deposit sebesar Rp. ' + sDepo + ',- untuk payment point ' + sPPID + '?';
if (window.confirm(s))
document.location = 'onpays-deposit-req-exec.php?_n=' + sNL + '&_i=' + sRID ;
}

function RejectDeposit(sRID, sPPID, sDepo, sNL)
{
var s = 'Anda menolak penambahan deposit sebesar Rp. ' + sDepo + ',- untuk payment point ' + sPPID + '?';
if (window.confirm(s))
document.location = 'onpays-deposit-req-exec-reject.php?_n=' + sNL + '&_i=' + sRID;
}

function ApproveBailoutDeposit(sRID, sPPID, sDepo, sNL)
{
var s = 'Anda menyetujui bailout deposit sebesar Rp. ' + sDepo + ',- untuk payment point ' + sPPID + '?';
if (window.confirm(s))
document.location = 'onpays-deposit-req-bailout-exec.php?_n=' + sNL + '&_i=' + sRID ;
}

function RejectBailoutDeposit(sRID, sPPID, sDepo, sNL)
{
var s = 'Anda menolak bailout deposit sebesar Rp. ' + sDepo + ',- untuk payment point ' + sPPID + '?';
if (window.confirm(s))
document.location = 'onpays-deposit-req-bailout-exec-reject.php?_n=' + sNL + '&_i=' + sRID;
}

function ExecReCheck(sPPID, sModuleName, sDateMin, sDateMax)
{
var s = 'Anda menyetujui rekap ulang payment point ' + sPPID + ', module '+ sModuleName +', mulai tanggal trx '+ sDateMin +' s/d '+ sDateMax +'?';
if (window.confirm(s))
	document.location = 'onpays-deposit-req-recheck-exec.php?_n=' + sModuleName + '&_i=' + sPPID + '&_dt='+sDateMin + '&_dtmax='+sDateMax;
}

function RejectReCheck(sPPID, sModuleName, sDateMin, sDateMax)
{
var s = 'Anda menolak rekap ulang payment point ' + sPPID + ', module '+ sModuleName +', mulai tanggal trx '+ sDateMin +' s/d '+ sDateMax +'?';

if (window.confirm(s)) 
	document.location = 'onpays-deposit-req-recheck-reject.php?_n=' + sModuleName + '&_i=' + sPPID + '&_dt='+sDateMin + '&_dtmax='+sDateMax;

}



function DeleteDeposit(sRID, sPPID, sAmount, iType)
{
  var bCancel = false;
  var iToken = 0;
  while ((!bCancel) && (iToken <= 0))
  {
    sToken = window.prompt("Masukkan token otentikasi ", "");
    if (sToken) {
      iToken = parseInt(sToken);
	}
    else bCancel = true;
    //alert(iDeposit);
  }
  if (iToken > 0)
  {
	  if (iType==0)
	  {
		  if (window.confirm("Are you sure to remove deposit for " + sPPID + " Rp. " + formatNumber(sAmount,',', '.', 0) + " ?"))
		  			window.open ('onpays-deposit-req-edit.php?_i=' + sRID + '&_v=' + iToken + '&_itype='+iType);
	  } else {
		  if (window.confirm("Are you sure to revise deposit for " + sPPID + " Rp. " + formatNumber(sAmount,',', '.', 0) + " ?"))
					window.open ('onpays-deposit-req-edit.php?_i=' + sRID + '&_v=' + iToken + '&_itype='+iType);
	  }
  }
  else alert('Revision canceled');
}

function doTransfer(sPPID, sAmount)
{
  var bCancel = false;
  var iToken = 0;
  var dDestPPID="";
  var dTrfAmount = 0;
  var dTrxMax = parseInt(sAmount);

  while ((!bCancel) && (iToken <= 0))
  {
    sToken = window.prompt("Input Authentication Token : ", "");
    if (sToken) {
      iToken = parseInt(sToken);
	  var bCancel1 = false;

      while ((!bCancel1) && (dDestPPID.length != 16)) {
	      dDestPPID = window.prompt("Input PPID Destination : ", "");
		  if ( (dDestPPID) && (dDestPPID.length == 16)) {
			  var bCancel2 = false;
			  while ((!bCancel2) && ((dTrfAmount > dTrxMax) || (dTrfAmount<=0) )) {
				  dTrfAmount = window.prompt("Amout will be transfered ? <= Rp. " + formatNumber(sAmount,',', '.', 0) + " ", "0");
				  if (dTrfAmount) {
					dTrfAmount = parseInt(dTrfAmount);			
				  }	else bCancel2 = true;
			  }
	      }	else bCancel1 = true;
	  }
	}else bCancel = true;
  }

  if ( !bCancel && !bCancel1 && !bCancel2 )
  {
	  if (window.confirm("Are you sure to transfer from " + sPPID + " to " + dDestPPID  + " Rp. " + formatNumber(dTrfAmount,',', '.', 0) + " ?"))
		  			window.open ('onpays-deposit-transfer.php?_sSource=' + sPPID + '&_v=' + iToken + '&_sDest='+dDestPPID+'&_dAmount='+dTrfAmount);
  } else {
	  alert('Transfer canceled');
  } 
}


function openFilterWindow(elem)
{
	window.open("parser/filter.php?id="+elem.id,"Jendela Filter","height=600,width=500,menubar=0,status=0,resizable=0,scrollbars=1");
}

function ResetReg(sPPID)
{
  var s = '';
  s = 'Anda yakin mau melakukan reset aktivasi ' + sPPID + ' ?';
  if (window.confirm(s))
    window.open('onpays-pp-reset.php?_p=' + sPPID);
}



function doSynchronized(sPPID, sAmount)
{
  var bCancel = false;
  var iToken = 0;
  var dDestPPID="";
  var dTrfAmount = parseInt(sAmount);

  while ((!bCancel) && (iToken <= 0))
  {
    sToken = window.prompt("Input Authentication Token : ", "");
    if (sToken) {
      iToken = parseInt(sToken);
	}
	else bCancel = true;
  }

  if ( !bCancel )
  {
	  if (window.confirm("Are you sure to synchronize " + sPPID + " Rp. " + formatNumber(dTrfAmount,',', '.', 0) + " ?")) {
                        document.forms['fsynchronize'].elements['_v'].value = iToken;
                        document.forms['fsynchronize'].elements['_va'].value = sPPID;
                        document.forms['fsynchronize'].elements['_val'].value = sAmount;
                        document.forms['fsynchronize'].submit();
 	  }

  } else {
	  alert('Transfer canceled');
  } 

}

function doDetail(sPPID, sDesc, sDate, sCid)
{
	  if (window.confirm("Are you sure to view " + sDesc + " for " + sPPID + " more detail?")) {
                        document.forms['fdetail'].elements['_ppid'].value = sPPID;
                        document.forms['fdetail'].elements['_desc'].value = sDesc;
                        document.forms['fdetail'].elements['_date'].value = sDate;
                        document.forms['fdetail'].elements['_cid'].value = sCid;
                        document.forms['fdetail'].submit();
 	  }

}

