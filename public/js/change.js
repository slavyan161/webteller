$(function() {
    if(!getCookie('monActive') || getCookie('monActive')=='null') {
        $('#monActive').val('prepaid');
    } else if(!getCookie('sFilter') || getCookie('sFilter')=='null') {
        $('#sFilter').val(getCookie('all'));
    } else {   
        $('#monActive').val(getCookie('monActive'));
        $('#sFilter').val(getCookie('sFilter'));
    }
    ctrlProduct.change();
});
ctrlProduct = {
    var_monActive: '',
    var_sFilter: '',
    change: function() {
        ctrlProduct.var_monActive = $('#monActive').val();
        ctrlProduct.var_sFilter = $('#sFilter').val();

        $.ajax({
            url: 'home/filter',
            type: 'POST',
            async: false,
            cache: true,
            data: {
                'monActive': ctrlProduct.var_monActive,
                'sFilter': ctrlProduct.var_sFilter
            },
            xhrFields: {
                withCredentials: false
            },
            //milisecond timeout
            timeout: 0,
            success: function(result) {
                //set cookie
                setCookie('monActive', ctrlProduct.var_monActive);
                setCookie('sFilter', ctrlProduct.var_sFilter);
                //
                jsonData = JSON.parse(result);
                for (key in jsonData) {
                    $.each(jsonData[key], function (k, arrayItem) {
                        if (k === 'showTrx') {
                            if (arrayItem.nPP > 0) {
                                document.getElementById('nPP').innerHTML = ': ' + arrayItem.nPP;
                            } else {
                                document.getElementById('nPP').innerHTML = ': Not Retrieved (Performance reason)';
                            }
                            TOTAL = parseFloat(arrayItem.TOTAL_TAG) + parseFloat(arrayItem.TOTAL_ADM);
                            document.getElementById('N_REK').innerHTML = ': ' + number_format(arrayItem.N_REK, 0, ',', '.');
                            document.getElementById('N_MONTH').innerHTML = ': ' + number_format(arrayItem.N_MONTH, 0, ',', '.');
                            document.getElementById('TOTAL_TAG').innerHTML = ': Rp. ' + number_format(arrayItem.TOTAL_TAG, 0, ',', '.') + ',-';
                            document.getElementById('TOTAL_ADM').innerHTML = ': Rp. ' + number_format(arrayItem.TOTAL_ADM, 0, ',', '.') + ',-';
                            document.getElementById('TOTAL').innerHTML = ': Rp. ' + number_format(TOTAL, 0, ',', '.') + ',-';
                        } else if (k === 'product') {
                            if (arrayItem.CSC_TD_DESC) {   
                                document.getElementById('prod').innerHTML = ': ' + arrayItem.CSC_TD_DESC;
                            } else {
                                document.getElementById('prod').innerHTML = ': ';
                            }
                        }
                        
                    });
                }

            },
            error: function (e) {
                console.debug(e);
            },
            complete: function (e) {
            }
        });
    }
};
