$(document).ready(function(){  
    $('#list-dataTable').jtable({
        title: '',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'id ASC', //Set default sorting
        selecting: true,
        actions: {
            listAction: 'http://localhost/webteller/pajakpbb/getDataPbb'
            // listAction: '{{ url.getBaseUri() ~ pajakpbb/getPbbBayar }}'
        },
        fields: {
            // no: {
            //     title: 'No',
            //     width: '2%',
            //     sorting: false
            // },
            // action: {
            //     title: 'Action',
            //     width: '18%',
            //     sorting: false
            // },
            namaArea: {
                title: 'Nama Area',
                width: '20%'
            },
            namaWP: {
                title: 'Nama WP',
                width: '20%'
            }
            // status_aktif: {
            //     title: 'Status',
            //     width: '10%',
            //     display: function (data) {
            //         console.log(data);
            //         return (data.record.status_aktif == 'f') ? '<span class="label label-danger">Not Active</span>' : '<span class="label label-success">Active</span>';
            //     }
            // }
        }
    });
    
    $('#list-dataTable').jtable('load'); //trigger load pertama
    $('#frm-filter').submit(function (e) {
        e.preventDefault();
        $('#list-dataTable').jtable('load', $('#frm-filter').serialize()); //Filter pencarian jtable
    });

    $('#refresh-filter').click(function (e) {
        e.preventDefault();
        $('#list-dataTable').jtable('load');

        $('#kode_filter').val('');
        $('#nama_filter').val('');
        $("#status_aktif_filter").val('').trigger('change');
    });

});