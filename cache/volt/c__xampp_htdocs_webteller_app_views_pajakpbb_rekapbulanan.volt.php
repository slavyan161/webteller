<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/bayar' ?>" >Bayar</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/cetakulang' ?>" >Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/laporanharian' ?>" >Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link active" href="<?= $this->url->getBaseUri() . 'pajakpbb/rekapbulanan' ?>" >Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/print' ?>">Test Print</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/daftar' ?>">Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_bulanan">
                    <form method="post" action="<?= $this->url->getBaseUri() . 'pdf/rekapbulananPdf' ?>" autocomplete="off">
                      <label for="example-date-input" class="col-0 col-form-label">Bulan Transaksi </label>
                      <div class="">
                        <?= $form->render('bulanTransaksi') ?>
                      </div>
                      &nbsp;
                       <label for="example-date-input" class="col-0 col-form-label">Tahun Transaksi </label>
                      <div class="">
                        <?= $form->render('tahunTransaksi') ?>
                      </div>
                      &nbsp;
                      <div class="form-group row btn-toolbar">
                      <?= $this->tag->submitButton(['Lihat', 'class' => 'btn btn-primary']) ?>
                      </div>
                    </form>
                    </div>

                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper