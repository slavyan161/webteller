<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item">
                      <a class="nav-link active" href="<?= $this->url->getBaseUri() . 'pajakpbb/bayar' ?>" >Bayar</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/cetakulang' ?>" >Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/laporanharian' ?>">Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/rekapbulanan' ?>" >Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/print' ?>" >Test Print</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/daftar' ?>" >Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab_bayar">
                      <form method="post" autocomplete="off">
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">NOP</label>
                          <input type="hidden" name="tabName" value="Bayar">
                          <div class="col-sm-10">
                            <?= $form->render('nop') ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Kode Area</label>

                          <div class="col-sm-10">
                            <?= $form->render('kodeArea') ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <?= $this->tag->submitButton(['cek', 'class' => 'btn btn-primary']) ?>
                          <?= $this->tag->submitButton(['kosongkan', 'class' => 'btn btn-primary']) ?>
                        </div>        
                      </form> 
                      <div>
                      <div id="list-dataTable">
                        <?php $totalTagihan = 0; ?>
                        <?php $v18987937111iterated = false; ?><?php $v18987937111iterator = $page->items; $v18987937111incr = 0; $v18987937111loop = new stdClass(); $v18987937111loop->self = &$v18987937111loop; $v18987937111loop->length = count($v18987937111iterator); $v18987937111loop->index = 1; $v18987937111loop->index0 = 1; $v18987937111loop->revindex = $v18987937111loop->length; $v18987937111loop->revindex0 = $v18987937111loop->length - 1; ?><?php foreach ($v18987937111iterator as $bayar) { ?><?php $v18987937111loop->first = ($v18987937111incr == 0); $v18987937111loop->index = $v18987937111incr + 1; $v18987937111loop->index0 = $v18987937111incr; $v18987937111loop->revindex = $v18987937111loop->length - $v18987937111incr; $v18987937111loop->revindex0 = $v18987937111loop->length - ($v18987937111incr + 1); $v18987937111loop->last = ($v18987937111incr == ($v18987937111loop->length - 1)); ?><?php $v18987937111iterated = true; ?>
                        <?php if ($v18987937111loop->first) { ?>
                        <table class="table table-hover box-body no-padding" align="center">
                            <thead class="thead-light">
                                <tr align="center">
                                    <th><input type="checkbox" onClick="toggle(this)" /><br/></th>
                                    <th><center>NOP</center></th>
                                    <th><center>Nama WP</center></th>
                                    <th><center>Tahun Pajak</center></th>
                                    <th><center>Total</center></th>
                                    <th><center>Status</center></th>
                                    <!-- <th><center>Link</center></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php } ?>
                                <form method ="post" action="statusBayar">
                                <tr align="center">
                                  <td><input type="checkbox" class="mt-1" name="foo"></td>
                                  <td><?= $bayar->nop ?></td>
                                  <input type="hidden" name="nop" value="<?= $bayar->nop ?>" class="form-control">
                                  <td><?= $bayar->namaWP ?></td>
                                  <input type="hidden" name="namaWP" value="<?= $bayar->namaWP ?>" class="form-control">
                                  <td><?= $bayar->tahunPajak ?></td>
                                  <td><?= 'Rp. ' . number_format($bayar->totalBayar, 2, '.', ' ') ?></td>
                                  <td>
                                    <input type="submit" value="Bayar" name="bayarButton" class="btn btn-xsm btn-outline-success">
                                    <input type="submit" value="Hapus" name="bayarButton" class="btn btn-xsm btn-outline-danger">
                                    <!-- <button type="button" class="btn btn-xsm btn-outline-danger">Hapus</button> -->
                                  </td>
                                  </form>
                                  <?php $totalTagihan += $bayar->totalBayar; ?>
                                </tr>
                                <?php if ($v18987937111loop->last) { ?>
                            </tbody>
                            <tr>
                                <td><?= $page->current ?> / <?= $page->total_pages ?></td>
                                <td colspan="10" align="right">
                                  <div class="btn-group">
                                    <?= $this->tag->linkTo(['bayar/index', '<i class="fa fa-fast-backward"></i> First', 'class' => 'btn btn-default']) ?>
                                    <?= $this->tag->linkTo(['bayar/index?page=' . $page->before, '<i class="fa fa-step-backward"></i> Previous', 'class' => 'btn btn-default']) ?>
                                    <?= $this->tag->linkTo(['bayar/index?page=' . $page->next, '<i class="fa fa-step-forward"></i> Next', 'class' => 'btn btn-default']) ?>
                                    <?= $this->tag->linkTo(['bayar/index?page=' . $page->last, '<i class="fa fa-fast-forward"></i> Last', 'class' => 'btn btn-default']) ?>
                                    <span class="help-inline"><?= $page->current ?>/<?= $page->total_pages ?></span>
                                  </div>
                                </td>
                            </tr>
                        </table>
                        <?php } ?>
                        <?php $v18987937111incr++; } if (!$v18987937111iterated) { ?>
                        No profiles are recorded
                        <?php } ?>
                      </div>
                        <br>
                        <form class="form-inline col-md-10 m-auto">
                          <label class="col-sm-2">Total Tagihan</label>
                          <input type="text" class="col-sm-2" value="<?= 'Rp. ' . number_format($totalTagihan, 2, '.', ' ') ?>" disabled>
                          <label class="col-sm-1">Bayar</label>
                          <input type="text" class="col-sm-2" value="2.500.000">
                          <label class="col-sm-1">Sisa</label>
                          <input type="text" class="col-sm-2" value="30.866" disabled>
                          <div class="col-sm-2">
                            <!-- <button type="button" class="btn btn-primary btn-sm" href="<?= $this->url->getBaseUri() . 'pajakpbb/bayarTagihan' ?>">Bayar</button> -->
                            <a href="<?= $this->url->getBaseUri() . 'pajakpbb/bayarTagihan' ?>" class="btn btn-xsm btn-primary">Bayar</a>
                            <!-- <input type="submit" value="Bayar" name="bayarButton" class="btn btn-xsm btn-outline-success"> -->

                          </div>
                        </form>
                      </div>     
                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    function toggle(source) {
      checkboxes = document.getElementsByName('foo');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
  }
  </script>