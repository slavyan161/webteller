<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/bayar' ?>" >Bayar</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/cetakulang' ?>" >Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/laporanharian' ?>" >Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/rekapbulanan' ?>" >Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?= $this->url->getBaseUri() . 'pajakpbb/print' ?>">Test Print</a></li>
                    <li class="nav-item"><a class="nav-link active" href="<?= $this->url->getBaseUri() . 'pajakpbb/daftar' ?>">Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                      <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_daftar_bayar">
                      <button type="submit" class="btn btn-primary mb-4 btn-toolbar">Tambah Group </button>
                      <div class="form-group row">
                        <label for="example-date-input" class="col-0 col-form-label">Cari Group</label>&nbsp;
                        <input class="form-control mb-4 col-4" id="cariGroup" type="text" placeholder=" ">&nbsp;
                        <button type="submit" class="btn btn-primary mb-4 btn-toolbar">Cari</button>
                      </div> 

                    </div>    
                    </div>

                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper