<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Web Teller | <?= ucwords($this->router->getControllerName()) ?></title>

  <?= $this->tag->stylesheetLink('css/font-awesome.css') ?>
  <?= $this->tag->stylesheetLink('css/app.css') ?>
  <?= $this->tag->stylesheetLink('css/adminlte.css') ?>


  <?= $this->assets->outputCss('headerCss') ?>
  <?= $this->assets->outputInlineCss('headerCss') ?>
  <!-- Add Js -->
  <?= $this->assets->outputJs('headerJs') ?>
  <?= $this->assets->outputInlineJs('headerJs') ?>
  <!-- Jtable Initalize -->
  <!-- <?= $this->tag->javascriptInclude('assets/jtables/jquery.jtable.min.js') ?> -->
  <!-- <?= $this->tag->javascriptInclude('js/jtablePbb.js') ?> -->

</head>
<body class="hold-transition skin-blue sidebar-mini login-page fixed">
  	<?= $this->getContent() ?>

    <!-- jQuery 3 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/jquery/dist/jquery.min.js') ?>
    <!-- jQuery UI 1.11.4 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/jquery-ui/jquery-ui.min.js') ?>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- JS Admin LTE BJB Web Teller Initialized By PajakpbbController.php -->
    <!-- <?= $this->tag->javascriptInclude('adminlte/js/jquery.js') ?>
    <?= $this->tag->javascriptInclude('adminlte/js/bootstrap.bundle.js') ?>
    <?= $this->tag->javascriptInclude('adminlte/js/adminlte.js') ?> -->

    <!-- Bootstrap 3.3.7 -->
    <?= $this->tag->javascriptInclude('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>

    <!-- JTABLE Initalize -->
    <!-- <?= $this->tag->javascriptInclude('assets/jtables/jquery.jtable.min.js') ?> -->
    <!-- <?= $this->tag->javascriptInclude('js/jtablePbb.js') ?> -->
    

    

    <!-- Add Js -->
    <?= $this->assets->outputJs('footerJs') ?>
    <?= $this->assets->outputInlineJs('footerJs') ?>
</body>
</html>