<?php
namespace App\Controllers;

use TCPDF;
use App\Forms\PajakpbbForm;
use App\Models\Bayar;
use App\Models\LaporanHarian;
use App\Models\RekapBulanan;
use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\View;

class PdfController extends ControllerBase
{

	public function initialize()
	{
		$this->view->setTemplateBefore('private');
		$collection = $this->assets();
	}

	public function indexAction()
	{
        // $this->view->pick('pdf/pdf');
	}
	public function laporanHarianPdfAction(){
		// dd($this->request->getPost());
		$totalJumlah = 0;
		$totalDenda = 0;

		setlocale(LC_ALL, 'IND');

		$numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\Laporanharian', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        // $laporanHarian = Laporanharian::find($parameters)->toArray();
        dd($parameters["conditions"]);
        // dd($parameters);
        $laporanHarian = Laporanharian::find($parameters);
        $laporanHarianArray = $laporanHarian->toArray();
        $count = count($laporanHarianArray);
        $pdfName = 'LaporanHarian.pdf';
        $htmlTag = "
        <h1 style = 'text-align: center;'><strong>Laporan Harian</strong></h1>
        <hr>
        <h3 style = 'text-align: center;'><strong>REKAPITULASI TRANSAKSI HARIAN PAJAK BUMI DAN BANGUNAN </strong></h3>
        <br>
        <table style='width:100%'>
          <tr>
            <td>No Cabang: </td>
            <td>Cabang Utama Bandung</td>
            <td>Tanggal : </td>
          </tr>
          <tr>
            <td>User : </td>
            <td>WS01</td>
            <td>Waktu : </td>
          </tr>
        </table>
        <br>
        <hr>
        <br>
        <table>
          <tr>
            <th>No</th>
            <th>Lembar Rekening</th>
            <th>Bulan Tagihan</th>
          </tr>
          <hr>
          ";
          
          for($i=0;$i<$count;$i++){
          	$no = $i+1;
          	$htmlTag .="
          	<tr>
          	<td>{$no}</td>
            <td>{$laporanHarianArray[$i]['lembarRekening']}</td>
            <td>{$laporanHarianArray[$i]['bulanTagihan']}</td>
          </tr>
          <hr>
          ";
          }
          $htmlTag .="</table>";
          $htmlTag .="
          <br>
          <br>
          <h3 style = 'text-align: center;'><strong>RINCIAN TRANSAKSI </strong></h3>
          <hr>
          <table>
          <tr>
            <th>No</th>
            <th>ID Pelanggan</th>
            <th>JML Trans</th>
            <th>Denda</th>
            <th>Jumlah</th>
            <th>Jumlah Calculated</th>
          </tr>
          <hr>
          ";
          for ($i=0; $i<$count ; $i++) { 
          	$no = $i+1;
          	$totalJumlah = $totalJumlah + $laporanHarianArray[$i]['jumlah'];
          	$totalDenda = $totalDenda + $laporanHarianArray[$i]['denda'];
          	$htmlTag .="
          	<tr>
          	<td>{$no}</td>
          	<td>{$laporanHarianArray[$i]['idPelanggan']}</td>
            <td>{$laporanHarianArray[$i]['jumlahTransaksi']}</td>
            <td>{$laporanHarianArray[$i]['denda']}</td>
            <td>{$laporanHarianArray[$i]['jumlah']}</td>
	          </tr>
	          <hr>";
          	}
          	$htmlTag .="
          	<br>
          	CATATAN
          	<hr>
          	<table style='width:100%';>
          	<tr>
          	<td>Rupiah Tagihan</td>
          	<td>{$totalJumlah}</td>
          	</tr>
          	<tr>
          	<td>Rupiah Denda</td>
          	<td>{$totalDenda}</td>
          	</tr>
          	 ";
      	$htmlTag .="</table>";
      	// dd($totalJumlah);
        $this->view->laporanHarian = $laporanHarian;
        $this->view->count = $count;
        $this->view->htmlTag = $htmlTag;
        $this->view->pdfName = $pdfName;
        $this->view->pick('pdf/laporanHarianPdf');
	}
	public function rekapbulananPdfAction(){
		// dd($this->request->getPost());
		$totalJumlah = 0;
		$totalDenda = 0;

		// setlocale(LC_ALL, 'IND');

		$numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\RekapBulanan', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        // dd($parameters);
        // $laporanHarian = Laporanharian::find($parameters)->toArray();
        $data = RekapBulanan::find($parameters)->toArray();
        // dd($data);
        if($data == NULL){
        	dd("dataKosong");
        }
        else
        {
	        $count = count($data);
	        $pdfName = 'RekapBulanan.pdf';
	        $htmlTag = "
	        <h1 style = 'text-align: center;'><strong>Laporan Harian</strong></h1>
	        <hr>
	        <h3 style = 'text-align: center;'><strong>REKAPITULASI TRANSAKSI BULANAN PAJAK BUMI DAN BANGUNAN </strong></h3>
	        <br>
	        <table style='width:100%'>
	          <tr>
	            <td>No Cabang: </td>
	            <td>Cabang Utama Bandung</td>
	            <td>Tanggal : </td>
	          </tr>
	          <tr>
	            <td>User : </td>
	            <td>WS01</td>
	            <td>Waktu : </td>
	          </tr>
	        </table>
	        <br>
	        <hr>
	        <br>
	        <table>
	          <tr>
	            <th>No</th>
	            <th>Lembar Rekening</th>
	            <th>Bulan Tagihan</th>
	          </tr>
	          <hr>
	          ";
	          
	          for($i=0;$i<$count;$i++){
	          	$no = $i+1;
	          	$htmlTag .="
	          	<tr>
	          	<td>{$no}</td>
	            <td>{$data[$i]['lembarRekening']}</td>
	            <td>{$data[$i]['bulanTagihan']}</td>
	          </tr>
	          <hr>
	          ";
	          }
	          $htmlTag .="</table>";
	          $htmlTag .="
	          <br>
	          <br>
	          <h3 style = 'text-align: center;'><strong>RINCIAN TRANSAKSI </strong></h3>
	          <hr>
	          <table>
	          <tr>
	            <th>No</th>
	            <th>ID Pelanggan</th>
	            <th>JML Trans</th>
	            <th>Denda</th>
	            <th>Jumlah</th>
	          </tr>
	          <hr>
	          ";
	          for ($i=0; $i<$count ; $i++) { 
	          	$no = $i+1;
	          	$totalJumlah = $totalJumlah + $data[$i]['jumlah'];
	          	$totalDenda = $totalDenda + $data[$i]['denda'];
	          	$htmlTag .="
	          	<tr>
	          	<td>{$no}</td>
	          	<td>{$data[$i]['idPelanggan']}</td>
	            <td>{$data[$i]['jumlahTransaksi']}</td>
	            <td>{$data[$i]['denda']}</td>
	            <td>{$data[$i]['jumlah']}</td>
		          </tr>
		          <hr>";
	          	}
	          	$htmlTag .="
	          	<br>
	          	CATATAN
	          	<hr>
	          	<table style='width:100%';>
	          	<tr>
	          	<td>Rupiah Tagihan</td>
	          	<td>{$totalJumlah}</td>
	          	</tr>
	          	<tr>
	          	<td>Rupiah Denda</td>
	          	<td>{$totalDenda}</td>
	          	</tr>
	          	 ";
	      	$htmlTag .="</table>";
	      	// dd($totalJumlah);
	        $this->view->data = $rekapBulanan;
	        $this->view->count = $count;
	        $this->view->htmlTag = $htmlTag;
	        $this->view->pdfName = $pdfName;
	        $this->view->pick('pdf/rekapBulananPdf');
        } //END IF DATA
	}

}

