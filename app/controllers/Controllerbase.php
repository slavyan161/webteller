<?php
namespace App\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use App\Models\Profiles;
class ControllerBase extends Controller
{
	public function assets()
	{
		//css
		$collection['headerCss'] = $this->assets->collection("headerCss");
		//js
		$collection['headerJs'] = $this->assets->collection("headerJs");
		$collection['footerJs'] = $this->assets->collection("footerJs");

		// $data = 5;
		return $collection;
	}

	public function beforeExecuteRoute(Dispatcher $dispatcher)
	{
		$collection = $this->assets();
	}
}