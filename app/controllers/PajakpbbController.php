<?php
namespace App\Controllers;

use App\Forms\PajakpbbForm;
use App\Forms\BayarForm;
use App\Forms\LaporanharianForm;
use App\Models\Bayar;
use App\Models\CetakUlang;
use App\Models\LaporanHarian;
use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\View;


class PajakpbbController extends Controllerbase
{

	public function initialize()
	{
		// $this->view->setTemplateAfter('pajakpbblayout');
		// $this->view->setLayout("pajakpbblayout");
		$this->view->setTemplateAfter('pajakpbblayout');
		$collection = $this->assets();
		// $collection['footerJs']->addJs('assets/jtables/jquery.jtable.min.js');
		// $collection['footerJs']->addJs('js/jtablePbb.js');

		$collection['footerJs']->addJs('adminlte/js/jquery.js');
		$collection['footerJs']->addJs('adminlte/js/bootstrap.bundle.js');
		$collection['footerJs']->addJs('adminlte/js/adminlte.js');

	}
	
	public function indexAction()
	{
		
	}
	public function bayarAction(){
		$this->persistent->searchParams = "";
		$numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\Bayar', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        // dd($parameters);
        $bayar = Bayar::find($parameters);

        if (count($bayar) == 0) {

            $this->flash->notice("The search did not find any Data");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }
        $paginator = new Paginator([
            "data" => $bayar,
            "limit" => 10,
            "page" => $numberPage
        ]);
        $this->view->page = $paginator->getPaginate();
        $form = new BayarForm();
        $form->clear();
        $this->view->form = new BayarForm(null);
        
	}
	public function statusBayarAction()
	{
		$parameters = $this->request->getPost("nop");
		dd($parameters);
		if($this->request->getPost("bayarButton") == "Bayar"){
			dd("Bayar TEST");
		}
		else
		{
			dd("Hapus TEST");
		}
	}
	public function bayarTagihanAction()
	{
		dd($this->request->getPost());
	}
	public function cetakulangAction(){
		$this->persistent->searchParams = "";
		$numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'App\Models\CetakUlang', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }
        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        
        $cetakUlang = CetakUlang::find($parameters);
        // dd($cetakUlang);
        if (count($cetakUlang) == 0) {

            $this->flash->notice("The search did not find any Data");
        }
        $paginator = new Paginator([
            "data" => $cetakUlang,
            "limit" => 10,
            "page" => $numberPage
        ]);
        $this->view->page = $paginator->getPaginate();
        $form = new PajakpbbForm();
        $form->clear();
        $this->view->form = new PajakpbbForm(null);
	}

	public function laporanharianAction(){
        $this->view->form = new LaporanharianForm(null);
	}
	public function rekapbulananAction(){
       $this->view->form = new PajakpbbForm();
	}
	public function printAction(){
       $this->view->form = new PajakpbbForm();
	}
	public function daftarAction(){
       $this->view->form = new PajakpbbForm();
	}

	public function getPbbBayarAction(){

		$bayar = Bayar::find();
		
		return json_encode([
				'Result' => "OK",
				'TotalRecordCount' => count($bayar),
				'Records' => $bayar
				]);
	}

}

