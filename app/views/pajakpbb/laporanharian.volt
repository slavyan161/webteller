                    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/bayar' }}">Bayar</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/cetakulang' }}" >Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link active" href="{{ url.getBaseUri() ~ 'pajakpbb/laporanharian' }}">Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/rekapbulanan' }}" >Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/print' }}" >Test Print</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/daftar' }}" >Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_harian">
                    <form method="post" action="{{ url.getBaseUri() ~ 'pdf/laporanHarianPdf' }}" autocomplete="off">
                     <div class="form-group">
                      <label for="exampleFormControlSelect1" class=" col-form-label">Kecamatan </label>
                      {{ form.render('kodeKecamatan') }}
                      <label for="exampleFormControlSelect2" class="col-form-label">Kelurahan </label>
                      {{ form.render('kodeKelurahan') }}
                      <label for="example-date-input" class="col-0 col-form-label">Date </label>
                       {{ form.render('tanggal') }}
                    </div>
                    <div class="form-group btn-toolbar">
                      {{ submit_button("Lihat", "class": "btn btn-primary") }}
                    </div>
                    </form>
                  </div>
                  </div>                    <!-- /.tab-pane -->
                  </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>