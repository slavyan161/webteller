<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/bayar' }}" >Bayar</a></li>
                    <li class="nav-item"><a class="nav-link active" href="{{ url.getBaseUri() ~ 'pajakpbb/cetakulang' }}"  >Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/laporanharian' }}" >Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/rekapbulanan' }}" >Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/print' }}" >Test Print</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb/daftar' }}" >Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="tab_cetak">
                      <form method="post" autocomplete="off">
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">NOP</label>

                          <div class="col-sm-10">
                            {{ form.render("nop") }}
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal</label>

                          <div class="col-sm-10">
                            {{ form.render("tanggalTransaksi") }}
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">BLTH</label>

                          <div class="col-sm-10">
                            {{ form.render("BLTH") }}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ submit_button("cek", "class": "btn btn-primary") }}
                          {{ submit_button("kosongkan", "class": "btn btn-primary") }}
                        </div>        
                      </form>     
                      <div>
                        {% for data in page.items %}
                        {% if loop.first %}
                        <table class="table table-hover box-body no-padding" align="center">
                            <thead class="thead-light">
                                <tr align="center">
                                    <th><input type="checkbox" onClick="toggle(this)" class="mt-1"></th>
                                    <th><center>Tgl Transaksi</center></th>
                                    <th><center>NOP</center></th>
                                    <th><center>Nama WP</center></th>
                                    <th><center>Jumlah Bulan</center></th>
                                    <th><center>BLTH</center></th>
                                    <th><center>Admin</center></th>
                                    <th><center>Total Tagihan</center></th>
                                    <th><center>Status</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                {% endif %}
                                <tr align="center">
                                  <td><input type="checkbox" class="mt-1" name="foo"></td>
                                  <td>{{ data.tanggalTransaksi }}</td>
                                  <td>{{ data.nop }}</td>
                                  <td>{{ data.namaWP }}</td>
                                  <td>{{ data.jumlahBulan }}</td>
                                  <td>{{ data.BLTH }}</td>
                                  <td>{{ 'Rp. ' ~ number_format(data.admin) }}</td>
                                  <td>{{ 'Rp. ' ~ number_format(data.totalTagihan) }}</td>
                                  <td>
                                    <button type="button" class="btn btn-xsm btn-outline-success">Cetak</button>
                                  </td>
                                </tr>
                                {% if loop.last %}
                            </tbody>
                            <tr>
                                <td>{{ page.current }} / {{ page.total_pages }}</td>
                                <td colspan="10" align="right">
                                  <div class="btn-group">
                                    {{ link_to("data/index", '<i class="fa fa-fast-backward"></i> First', "class": "btn btn-default") }}
                                    {{ link_to("data/index?page=" ~ page.before, '<i class="fa fa-step-backward"></i> Previous', "class": "btn btn-default") }}
                                    {{ link_to("data/index?page=" ~ page.next, '<i class="fa fa-step-forward"></i> Next', "class": "btn btn-default") }}
                                    {{ link_to("data/index?page=" ~ page.last, '<i class="fa fa-fast-forward"></i> Last', "class": "btn btn-default") }}
                                  </div>
                                    <!-- <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span> -->
                                </td>
                            </tr>
                        </table>
                        {% endif %}
                        {% else %}
                        No profiles are recorded
                        {% endfor %}
                      </div>     
                    </div>
                    
                    
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    function toggle(source) {
      checkboxes = document.getElementsByName('foo');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
  }
  </script>