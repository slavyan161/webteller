<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Web Teller | {{ router.getControllerName()|capitalize }}</title>

  {{ stylesheet_link('css/font-awesome.css') }}
  {{ stylesheet_link('css/app.css') }}
  {{ stylesheet_link('css/adminlte.css') }}


  {{ this.assets.outputCss('headerCss') }}
  {{ this.assets.outputInlineCss('headerCss') }}
  <!-- Add Js -->
  {{ this.assets.outputJs('headerJs') }}
  {{ this.assets.outputInlineJs('headerJs') }}
  <!-- Jtable Initalize -->
  <!-- {{ javascript_include('assets/jtables/jquery.jtable.min.js') }} -->
  <!-- {{ javascript_include('js/jtablePbb.js') }} -->

</head>
<body class="hold-transition skin-blue sidebar-mini login-page fixed">
  	{{ content() }}

    <!-- jQuery 3 -->
    {{ javascript_include('assets/bower_components/jquery/dist/jquery.min.js') }}
    <!-- jQuery UI 1.11.4 -->
    {{ javascript_include('assets/bower_components/jquery-ui/jquery-ui.min.js') }}
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- JS Admin LTE BJB Web Teller Initialized By PajakpbbController.php -->
    <!-- {{ javascript_include('adminlte/js/jquery.js') }}
    {{ javascript_include('adminlte/js/bootstrap.bundle.js') }}
    {{ javascript_include('adminlte/js/adminlte.js') }} -->

    <!-- Bootstrap 3.3.7 -->
    {{ javascript_include('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}

    <!-- JTABLE Initalize -->
    <!-- {{ javascript_include('assets/jtables/jquery.jtable.min.js') }} -->
    <!-- {{ javascript_include('js/jtablePbb.js') }} -->
    

    

    <!-- Add Js -->
    {{ this.assets.outputJs('footerJs') }}
    {{ this.assets.outputInlineJs('footerJs') }}
</body>
</html>