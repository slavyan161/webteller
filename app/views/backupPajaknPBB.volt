<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pajak Bumi dan Bangunan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                  <ul class="nav nav-tabs card-header-tabs">
                    <!-- <li class="nav-item"><a class="nav-link active" href="#tab_bayar" data-toggle="tab">Bayar</a></li> -->
                    <li class="nav-item">
                      <a class="nav-link active" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_bayar' }}" data-toggle="tab" >Bayar</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_cetak' }}"  data-toggle="tab">Cetak Ulang</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_harian' }}"  data-toggle="tab">Laporan Harian</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_bulanan' }}"  data-toggle="tab">Rekap Bulanan</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_print' }}"  data-toggle="tab">Test Print</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ url.getBaseUri() ~ 'pajakpbb#tab_daftar' }}"  data-toggle="tab">Daftar Bayar</a></li>
                    
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab_bayar">
                      <form method="post" autocomplete="off">
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">NOP</label>
                          <input type="hidden" name="tabName" value="Bayar">
                          <div class="col-sm-10">
                            {{ form.render("nop") }}
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Kode Area</label>

                          <div class="col-sm-10">
                            {{ form.render("kodeArea") }}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ submit_button("cek", "class": "btn btn-primary") }}
                          {{ submit_button("kosongkan", "class": "btn btn-primary") }}
                        </div>        
                      </form> 
                      <div>
                      <div id="list-dataTable">
                        {% set totalTagihan = 0 %}
                        {% for bayar in page.items %}
                        {% if loop.first %}
                        <table class="table table-hover box-body no-padding" align="center">
                            <thead class="thead-light">
                                <tr align="center">
                                    <th><input type="checkbox" class="mt-1"></th>
                                    <th><center>NOP</center></th>
                                    <th><center>Nama WP</center></th>
                                    <th><center>Tahun Pajak</center></th>
                                    <th><center>Total</center></th>
                                    <th><center>Status</center></th>
                                    <!-- <th><center>Link</center></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                {% endif %}
                                <tr align="center">
                                  <td><input type="checkbox" class="mt-1"></td>
                                  <td>{{ bayar.nop }}</td>
                                  <td>{{ bayar.namaWP }}</td>
                                  <td>{{ bayar.tahunPajak }}</td>
                                  <td>{{ 'Rp. ' ~ number_format(bayar.totalBayar) }}</td>
                                  <td>
                                    <button type="button" class="btn btn-xsm btn-outline-success">Bayar</button>
                                    <button type="button" class="btn btn-xsm btn-outline-danger">Hapus</button>
                                  </td>
                                  {% set totalTagihan += bayar.totalBayar %}
                                </tr>
                                {% if loop.last %}
                            </tbody>
                            <tr>
                                <td>{{ page.current }} / {{ page.total_pages }}</td>
                                <td colspan="10" align="right">
                                  <div class="btn-group">
                                    {{ link_to("bayar/index", '<i class="fa fa-fast-backward"></i> First', "class": "btn btn-default") }}
                                    {{ link_to("bayar/index?page=" ~ page.before, '<i class="fa fa-step-backward"></i> Previous', "class": "btn btn-default") }}
                                    {{ link_to("bayar/index?page=" ~ page.next, '<i class="fa fa-step-forward"></i> Next', "class": "btn btn-default") }}
                                    {{ link_to("bayar/index?page=" ~ page.last, '<i class="fa fa-fast-forward"></i> Last', "class": "btn btn-default") }}
                                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                                  </div>
                                </td>
                            </tr>
                        </table>
                        {% endif %}
                        {% else %}
                        No profiles are recorded
                        {% endfor %}
                      </div>
                        <br>
                        <form class="form-inline col-md-10 m-auto">
                          <label class="col-sm-2">Total Tagihan</label>
                          <input type="text" class="col-sm-2" value="{{ 'Rp. ' ~ number_format(totalTagihan) }}" disabled>
                          <label class="col-sm-1">Bayar</label>
                          <input type="text" class="col-sm-2" value="2.500.000">
                          <label class="col-sm-1">Sisa</label>
                          <input type="text" class="col-sm-2" value="30.866" disabled>
                          <div class="col-sm-2">
                            <button type="button" class="btn btn-primary btn-sm">Bayar</button>
                          </div>
                        </form>
                      </div>     
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_cetak">
                      <form method="post" autocomplete="off">
                        <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-2 col-form-label">NOP</label>

                          <div class="col-sm-10">
                            {{ form.render("nop") }}
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal</label>

                          <div class="col-sm-10">
                            {{ form.render("tanggal") }}
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPassword3" class="col-sm-2 col-form-label">BLTH</label>

                          <div class="col-sm-10">
                            {{ form.render("blth") }}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ submit_button("cek", "class": "btn btn-primary") }}
                          {{ submit_button("kosongkan", "class": "btn btn-primary") }}
                        </div>        
                      </form>     
                      <div>
                        <table class="table table-hover">
                          <thead class="thead-light">
                            <tr>
                              <th><input type="checkbox" class="mt-1"></th>
                              <th>Tgl Transaksi</th>
                              <th>NOP</th>
                              <th>Nama WP</th>
                              <th>Jumlah Bulan</th>
                              <th>BLTH</th>
                              <th>Admin</th>
                              <th>Total Tagihan</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>28-06-2018</td>
                              <td>123456789012345</td>
                              <td>Contoh Nama</td>
                              <td>3</td>
                              <td>MAR2018, APR2018, MEI2018</td>
                              <td>5.000</td>
                              <td>1.234.567</td>
                              <td class="td-actions">
                                <button type="button" class="btn btn-xsm btn-outline-success">Cetak</button>
                              </td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>28-06-2018</td>
                              <td>123456789012346</td>
                              <td>Contoh Nama 2</td>
                              <td>3</td>
                              <td>MAR2018, APR2018, MEI2018</td>
                              <td>5.000</td>
                              <td>1.234.567</td>
                              <td class="td-actions">
                                <button type="button" class="btn btn-xsm btn-outline-success">Cetak</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <button type="button" class="btn btn-primary">Cetak Data yang Dipilih</button>
                      </div>     
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_harian">
                      <form method="post">
                     <div class="form-group">
                      <label for="exampleFormControlSelect1" class=" col-form-label">Kecamatan </label>
                      {{ form.render('kecamatan') }}
                      <label for="exampleFormControlSelect2" class="col-1 col-form-label">Kelurahan </label>
                      {{ form.render('kelurahan') }}
                      <label for="example-date-input" class="col-0 col-form-label">Date </label>
                       {{ form.render('tanggal') }}
                       <input type="hidden" name="tabName" value="laporanHarian">
                    </div>
                    <div class="form-group btn-toolbar">
                      {{ submit_button("Lihat", "class": "btn btn-primary") }}
                    </div>
                    </form>
                     <br>
                     <hr/>      
                     <br>
                     <font size="5"><center><b>REKAPITULASI TRANSAKSI HARIAN PAJAK BUMI DAN BANGUNAN</b></center></font>
                     <br>
                   
                     {% for laporanHarian in harianPage.items %}
                     {% if loop.first %}
                     <div class="d-flex justify-content-between">
                          <div>
                            <font size="4">
                             No Cabang : 0001 <br>
                             User : 34
                             
                          </div>
                          <div>
                             Cabang Utama Bandung<br>
                             WS01
                          </div>
                          <div>
                             Tanggal : {{ laporanHarian.tanggal }} <br>
                             Waktu : 15:06:50
                             </font>
                          </div>
                     </div>
                     <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Lembar Rekening</th>
                            <th scope="col">Bulan Tagihan</th>
                          </tr>
                        </thead>
                        {% endif %}
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>{{ laporanHarian.lembarRekening }}</td>
                            <td>{{ laporanHarian.bulanTagihan }}</td>
                          </tr>
                        </tbody>
                      </table>

                     <br>
                     {% if loop.first %}
                     <center><b>RINCIAN TRANSAKSI</b></center>
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Pelanggan</th>
                            <th scope="col">JML Trans</th>
                            <th scope="col">Denda</th>
                            <th scope="col">Jumlah</th>
                          </tr>
                        </thead>
                        {% endif %}
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>{{ laporanHarian.idPelanggan }}</td>
                            <td>{{ laporanHarian.jumlahTransaksi }}</td>
                            <td>{{ 'Rp. ' ~ number_format(laporanHarian.denda) }}</td>
                            <td>{{ 'Rp. ' ~ number_format(laporanHarian.jumlah) }}</td>
                          </tr>
                        </tbody>
                      </table>
                    <hr/>
                    CATATAN <br>
                    <hr/>
                    <div class="d-flex justify-content-between">
                          <div>
                             Rupiah Tagihan<br>
                             Rupiah Denda

                          </div>
                          <div>
                             {{ 'Rp. ' ~ number_format(laporanHarian.jumlah) }}<br>
                              {{ 'Rp. ' ~  number_format(laporanHarian.denda) }}
                          </div>
                     </div>
                    <hr/>
                    <br>
                    <div class="d-flex justify-content-between">
                          <div>
                             <b>JUMLAH YANG HARUS DI SETORKAN</b><br>
                             <b>TERBILANG :</b> Tiga Puluh satu juta dua puluh satu ribu seratus lima puluh empat rupiah

                          </div>
                          <div>
                             <b>Rp.    {{ number_format (laporanHarian.jumlah - laporanHarian.denda) }}</b><br>

                          </div>
                     </div>
                      <hr/>
                      {% endfor %}
                    </div>
                    <!-- /.tab-pane -->
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_bulanan">
                    <div class="form-group row btn-toolbar">
                      <label for="example-date-input" class="col-0 col-form-label">Bulan Transaksi </label>
                      <div class="col-2">
                        {{ form.render('Bulan') }}
                      </div>
                      &nbsp;
                      <select class="form-control col-2" id="exampleFormControlSelect2">
                        <option>2018</option>
                        <option>2017</option>
                        <option>2016</option>
                        <option>2015</option>
                        <option>2014</option>
                      </select>
                      &nbsp;
                      <button type="submit" class="btn btn-primary mb-4 btn-toolbar">Lihat </button> &nbsp;
                      <button type="submit" class="btn btn-primary mb-4 btn-toolbar">Print </button> &nbsp;
                      <button type="submit" class="btn btn-primary mb-4 btn-toolbar">Unduh Detail </button> 
                    </div>
                    <hr/>
                    <font size="5"><center><b>REKAPITULASI TRANSAKSI HARIAN PAJAK BUMI DAN BANGUNAN</b></center></font>
                    <br>
                    <div class="d-flex justify-content-between">
                          <div>
                            <font size="4">
                             No Cabang : 0001 <br>
                             User : 34
                             
                          </div>
                          <div>
                             Cabang Utama Bandung<br>
                             WS01
                          </div>
                          <div>
                             Tanggal : 09/07/2018 <br>
                             Waktu : 15:06:50
                             </font>
                          </div>
                     </div>
                     <br>
                     <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Lembar Rekening</th>
                            <th scope="col">Bulan Tagihan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>1245</td>
                            <td>0</td>
                          </tr>
                        </tbody>
                      </table>
                     <br>
                     <center><b>RINCIAN TRANSAKSI</b></center>
                     <br>
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID Pelanggan</th>
                            <th scope="col">JML Trans</th>
                            <th scope="col">Denda</th>
                            <th scope="col">Jumlah</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>VIXXXXXX</td>
                            <td>1245</td>
                            <td>0,00,-</td>
                            <td>31.021.154,00</td>
                          </tr>
                        </tbody>
                      </table>
                    <br>
                    <hr/>
                    CATATAN <br>
                    <hr/>
                    <div class="d-flex justify-content-between">
                          <div>
                             Rupiah Tagihan<br>
                             Rupiah Denda

                          </div>
                          <div>
                             31.021.154,00,-<br>
                              0,00,-
                          </div>
                     </div>
                    <hr/>
                    <br>
                    <div class="d-flex justify-content-between">
                          <div>
                             <b>JUMLAH YANG HARUS DI SETORKAN</b><br>
                             <b>TERBILANG :</b> Tiga Puluh satu juta dua puluh satu ribu seratus lima puluh empat rupiah

                          </div>
                          <div>
                             <b>Rp.    31.021.154,00,-</b><br>

                          </div>
                     </div>
                      <hr/>

                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div><!-- /.card -->
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper