<?php
namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use App\Models\Bayar;
use App\Models\Kelurahan;
use App\Models\Kecamatan;

class PajakpbbForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id', ['class' => 'form-control']);
        }

        $this->add($id);

        $nop = new Text('nop', [
            'placeholder' => 'Nomor Objek Pajak',
            'class' => 'form-control'
        ]);

        $nop->addValidators([
            new PresenceOf([
                'message' => 'The nop is required'
            ])
        ]);

        $this->add($nop);

        $this->add(
            new Date(
                'tanggal' , ['class' => 'form-control']
            )
        );
        $this->add(
            new Date(
                'tanggalTransaksi' , ['class' => 'form-control']
            )
        );
        $this->add(
             new Text('BLTH' , 
                ['placeholder' => 'BLTH','class' => 'form-control']
            )
        );
        $this->add(
             new Text('carigroup' , 
                ['placeholder' => '','class' => 'form-control']
            )
        );

        // $bayarCheck = Bayar::find([
        //     'column' => 'id,nop'
        // ]);
        // foreach ($bayarCheck as $key => $value) {
        //     $this->add(new Check('idBayar[]', array('value' => $value->id)));
        // }
        $bayar = Bayar::find([
            'column' => 'kodeArea,namaArea'
        ]);

        $this->add(new Select('kodeArea', $bayar, [
            'using' => [
                'kodeArea',
                'namaArea'
            ],
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control'
        ]));

        $kecamatan = Kecamatan::find();
        $this->add(new Select('kodeKecamatan',$kecamatan,[
           'using' => [
                'id',
                'namaKecamatan'
            ], 
        'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control'
        ]));

        $kelurahan = Kelurahan::find();
        $this->add(new Select('kodeKelurahan',$kelurahan,[
           'using' => [
                'id',
                'namaKelurahan'
            ], 
        'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control'
        ]));

       $this->add(new Select('bulanTransaksi', [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        ],['useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control']));

       $this->add(new Select('tahunTransaksi', [
            '2016' => '2016',
            '2017' => '2017',
            '2018' => '2018'
        ], ['useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control']));
    }
}
