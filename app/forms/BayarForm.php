<?php
namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use App\Models\Bayar;
class BayarForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id', ['class' => 'form-control']);
        }

        $this->add($id);

        $nop = new Text('nop', [
            'placeholder' => 'Nomor Objek Pajak',
            'class' => 'form-control'
        ]);

        $nop->addValidators([
            new PresenceOf([
                'message' => 'The nop is required'
            ])
        ]);

        $this->add($nop);

        $bayar = Bayar::find([
            'column' => 'kodeArea,namaArea'
        ]);

        $this->add(new Select('kodeArea', $bayar, [
            'using' => [
                'kodeArea',
                'namaArea'
            ],
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => '',
            'class' => 'form-control'
        ]));
    }
}
