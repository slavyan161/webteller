<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class CetakUlang extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $nop;
    public $tanggalTransaksi;
    public $namaWP;
    public $jumlahBulan;
    public $BTLH;
    public $admin;
    public $totalTagihan;
    public $status;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}