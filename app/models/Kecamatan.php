<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Kecamatan extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $namaKecamatan;
 

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}