<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use App\Models\DataPermissions;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Bayar extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $nop;
    public $kodeArea;
    public $namaArea;
    public $namaWP;
    public $tahunPajak;
    public $status;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}