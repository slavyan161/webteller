<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class RekapBulanan extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $idPelanggan;
    public $jumlahTransaksi;
    public $lembarRekening;
    public $bulanTagihan;
    public $denda;
    public $jumlah;
    public $bulanTransaksi;
    public $tahunTransaksi;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}