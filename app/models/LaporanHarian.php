<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class LaporanHarian extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $kodeKecamatan;
    public $kodeKelurahan;
    public $tanggal;
    public $lembarRekening;
    public $bulanTagihan;
    public $idPelanggan;
    public $jumlahTransaksi;
    public $denda;
    public $jumlah;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}