<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * App\Models\Menus
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Kelurahan extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;
    public $namaKelurahan;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
  
    }

}